package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "<h3>Home sweet Home</h3>")
}

func contact(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "contact me : "+"<a>laurent.tonoudo.dev@gmail.com</a>")
}
func faq(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "<h1>FAQ</h1><br><form style='display:flex;flex-direction:column;'><input type='text' placeholder='firstname'><input type='text' placeholder='Lastname'><textarea placeholder='ask your question here...'></textarea></form>")
}
func error(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-type", "text/html")
	fmt.Fprint(w, "<h1>ERROR ERROR ERROR ERROR ERROR ERROR ERROR</h1>")
}

/* GORILLA/mux router */
func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", home)
	r.HandleFunc("/contact", contact)
	r.HandleFunc("/FAQ", faq)
	var e http.Handler = http.HandlerFunc(error)
	r.NotFoundHandler = e
	http.ListenAndServe(":3000", r)
}
