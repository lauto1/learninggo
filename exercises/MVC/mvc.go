package main

import (
	"html/template"
	"os"
)

func main() {
	//
	t, err := template.ParseFiles("mvc.gohtml")
	if err != nil {
		panic(err)
	}

	Data := struct {
		Subject   string
		Response1 string
		Response2 string
		Response3 string
	}{
		Subject:   "le MVC",
		Response1: "MODEL VIEW CONTROLLER",
		Response2: "Le model récupère les données via l'api ou la DB. Le controller gère les actions a effectuer avec celles-ci. La View affiche le contenu coté utilisateur. ",
		Response3: "cf Response1"}

	err = t.Execute(os.Stdout, Data)
	if err != nil {
		panic(err)
	}

}
